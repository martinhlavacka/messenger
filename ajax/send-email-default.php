<?php
	
	// STORE API KEY
    $api_key = $_POST['api_key'];

    // STORE SUBJECT AND BODY OF THE MESSAGE
	$email_subject = $_POST['subject'];
	$email_message = $_POST['message'];

    // STORE DETA ABOUT SENDER
    $sender_name = $_POST['sender_name'];
    $sender_email = $_POST['sender_email'];

    // STORE DATA ABOUT RECEIVER
    $receiver_name = $_POST['receiver_name'];
    $receiver_email = $_POST['receiver_email'];
                
    // LOAD REQUIRED CLASSES
    require 'libraries/emails/vendor/autoload.php';
        
    // SETUP API KEY
    $apiKey = $api_key;
            
    // SETUP SENDER AND RECEIVER
    $from = new SendGrid\Email($sender_name, $sender_email);
    $to = new SendGrid\Email($receiver_name, $receiver_email);
            
    // SETUP SUBJECT
    $subject = $email_subject;
        
    // SETUP CONTENT
    $content = new SendGrid\Content("text/html", $email_message);
            
    // TRY TO SEND E-MAIL
    $mail = new SendGrid\Mail($from, $subject, $to, $content);
    $sg = new \SendGrid($apiKey);
    $response = $sg->client->mail()->send()->post($mail);
    $result = $response->statusCode();

    // IF E-MAIL WAS SENT
    if($result == 202){
        echo "<div id='message-success'><div class='container'>E-Mail was succesfully sent!</div></div>";
    }

    // IF SENDING FAILED
    else{
        echo "<div id='message-error'><div class='container'>Error occured while sending e-mail!</div></div>";
    }
     
?>