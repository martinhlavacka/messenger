<?php
	
	// STORE PASSED VALUES
	$sms_message        = $_POST['message'];
    $sender_name        = $_POST['sender_name'];
    $sender_number      = $_POST['sender_number'];
    $receiver_number    = $_POST['receiver_number'];
    $api_key            = $_POST['api_key'];
    $api_secret         = $_POST['api_secret'];

    // IF PHONE NUMBER IS SET
    if(isset($_POST['sender_number']) && !empty($_POST['sender_number'])){

        // SET SENDER AS PHONE NUMBER
        $sender = $_POST['sender_number'];

    }

    // IF NAME IS SET
    else{

        // SET SENDER AS STRING
        $sender = $_POST['sender_name'];

    }

    // PREPARE SMS
    $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
        'api_key' => $api_key,
        'api_secret' => $api_secret,
        'to' => $_POST['receiver_number'],
        'from' => $sender,
        'text' => $sms_message
    ]);

    // TRY TO SEND SMS
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);

    // IS SMS WAS SUCCESFULLY SENT
    if (strpos($response, '"status": "0"') !== false) {
        echo "<div id='message-success'><div class='container'>SMS was succesfully sent!</div></div>";
    }

    // IF SENDING FAILED
    else{
        echo "<div id='message-error'><div class='container'>Error occured while sending message!</div></div>";
    }
      
?>