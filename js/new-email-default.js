// ON REQUEST TO SEND
$("#send-email").click(function(){

	// STORE VALUES
	var api_key = $("#api-key-email").val();
	var subject = $("#subject").val();
	var message = $("#message-unique").val();
	var sender_email 	= $("#sender-email").val();
	var sender_name		= $("#sender-name-unique").val();
	var receiver_email	= $("#receiver-email").val();
	var receiver_name	= $("#receiver-name").val();

	// MAKE AJAX CALL
	$.ajax({

		// SPECIFY REQUEST TYPE
	    url: "ajax/send-email-default.php",
	    type: "POST",

	    // SEND DATA
	    data:{
	    	subject: subject,
	    	message: message,
	    	sender_email: sender_email,
	    	sender_name: sender_name,
	    	receiver_email: receiver_email,
	    	receiver_name: receiver_name,
	    	api_key: api_key
	    },
	    
	    // IF REQUEST SUCCEEDED
	    success: function (response) {
	    	$("#result").show();
	   		$("#result").html(response);
	   		$("#result").delay('1500').fadeOut();
	    },

	    // IF REQUEST FAILED
	    error: function(response) {
	        $("#result").html("<div id='message-error'>" + response + "</div>");
	    }

	});

});