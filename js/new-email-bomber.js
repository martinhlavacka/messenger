// ON REQUEST TO START BOMBING
$("#send-email").click(function(){

	// STORE VALUES
	var api_key = $("#api-key-email").val();
	var sender_email = $("#sender-email").val();
	var sender_name	= $("#sender-name").val();
	var receiver_email = $("#receiver-email").val();
	var receiver_name = $("#receiver-name").val();
	var messages = $("#number-mail").val();

	// MAKE AJAX CALL
	$.ajax({

		// SPECIFY REQUEST TYPE
	    url: "ajax/send-email-bomber.php",
	    type: "POST",

	    // SEND DATA
	    data:{
	    	sender_email: sender_email,
	    	sender_name: sender_name,
	    	receiver_email: receiver_email,
	    	receiver_name: receiver_name,
	    	api_key: api_key,
	    	messages: messages
	    },
	    
	    // IF REQUEST SUCCEEDED
	    success: function (response) {
	    	$("#result").show();
	   		$("#result").html(response);
	   		$("#result").delay('1500').fadeOut();
	    },

	    // IF REQUEST FAILED
	    error: function(response) {
	        $("#result").html("<div id='message-error'>" + response + "</div>");
	    }

	});

});