// ON CHANFE OF SENDER´S NAME
$("#sender-name").keyup(function(){

	// REMOVE SENDER´S NUMBER
	$("#sender-number").val("");

});

// ON CHANGE OF SENDER´S NUMBER
$("#sender-number").keyup(function(){

	// REMOVE SENDER´S NAME
	$("#sender-name").val("");
	
});

// ON REQUEST TO START BOMBING
$("#send-sms").click(function(){

	// STORE VALUES
	var receiver_number	= $("#receiver-number").val();
	var api_key 		= $("#api-key").val();
	var api_secret 		= $("#api-secret").val();
	var messages		= $("#number-of-sms").val();

	// MAKE AJAX CALL
	$.ajax({

		// SPECIFY REQUEST TYPE
	    url: "ajax/send-sms-bomber.php",
	    type: "POST",

	    // SEND DATA
	    data:{
	    	receiver_number: receiver_number,
	    	api_key: api_key,
	    	api_secret: api_secret,
	    	messages: messages
	    },
	    
	    // IF REQUEST SUCCEEDED
	    success: function (response) {
	   		$("#result").show();
	   		$("#result").html(response);
	   		$("#result").delay('1500').fadeOut();
	    },

	    // IF REQUEST FAILED
	    error: function(response) {
	        $("#result").html("<div id='message-error'>" + response + "</div>");
	    }

	});

});