// ON CHANGE OF SENDER´S NAME
$("#sender-name").keyup(function(){

	// REMOVE SENDER´S NUMBER
	$("#sender-number").val("");

});

// ON CHANGE OF SENDER´S NUMBER
$("#sender-number").keyup(function(){

	// REMOVE SENDER´S NAME
	$("#sender-name").val("");
	
});

// ON CLICKING BUTTON TO SEND MESSAGE
$("#send-sms").click(function(){

	// STORE VALUES
	var sender_name 	= $("#sender-name").val();
	var sender_number	= $("#sender-number").val();
	var receiver_number	= $("#receiver-number").val();
	var message 		= $("#message").val();
	var api_key 		= $("#api-key").val();
	var api_secret 		= $("#api-secret").val();

	// MAKE AJAX CALL
	$.ajax({

		// SPECIFY REQUEST TYPE
	    url: "ajax/send-sms-default.php",
	    type: "POST",

	    // SEND DATA
	    data:{
	    	message: message,
	    	sender_number: sender_number,
	    	sender_name: sender_name,
	    	receiver_number: receiver_number,
	    	api_key: api_key,
	    	api_secret: api_secret
	    },
	    
	    // IF REQUEST SUCCEEDED
	    success: function (response) {
	   		$("#result").show();
	   		$("#result").html(response);
	   		$("#result").delay('1500').fadeOut();
	    },

	    // IF REQUEST FAILED
	    error: function(response) {
	        $("#result").html("<div id='message-error'>" + response + "</div>");
	    }

	});

});