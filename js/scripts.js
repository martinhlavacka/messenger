// ON PAGE LOAD
$( document ).ready(function() {

	// LOAD SIMPLE SENDER
    $("#page-content").load('elements/index-default.html'); 

});

// ON CLICKING THE BOMBER IN NAVIGATION
$("#bomber").click(function(){

	// LOAD BOMBER FORM
	$("#page-content").load('elements/index-bomber.html'); 

	// REMOVE ACTIVE CLASS
	$(".active-page").removeClass('active-page');

	// ADD ACTIVE CLASS
	$("#bomber").addClass('active-page');

	// FIX FOOTER´S POSITION
	$("footer").addClass('footer-fix');

});

// ON CLICKING SIMPLE MESSENGER IN NAVIGATION
$("#simple").click(function(){

	// LOAD SIMPLE FORM
	$("#page-content").load('elements/index-default.html'); 

	// REMOVE ACTIVE CLASS
	$(".active-page").removeClass('active-page');

	// ADD ACTIVE CLASS
	$("#simple").addClass('active-page');

	// RELEASE FOOTER´S POSITION
	$("footer").removeClass('footer-fix');

});

// ON CLICKING MOBILE NAVIGATION
$(".mobile-opener").click(function(){
	$(".mobile-content").toggle();
});

$("#simple-sms-mobile").click(function(){
	$("footer").removeClass('fixed-footer-mobile');
	$("#page-content").load('elements/index-default.html'); 
	$(".subtitle-mobile").html(" | Simple mode");
	$(".mobile-content").toggle();
});

$("#bomber-sms-mobile").click(function(){
	$("footer").addClass('fixed-footer-mobile');
	$("#page-content").load('elements/index-bomber.html'); 
	$(".subtitle-mobile").html(" | Bomber mode");
	$(".mobile-content").toggle();
});
